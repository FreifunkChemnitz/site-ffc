# SPDX-License-Identifier: Unlicense
# SPDX-FileCopyrightText: 2023 Freifunk Chemnitz e.V. <info@chemnitz.freifunk.net>

GLUON_RELEASE ?= $(DEFAULT_GLUON_RELEASE)-multi

GLUON_AUTOUPDATER_BRANCH ?= stable
GLUON_AUTOUPDATER_ENABLED ?= 1
GLUON_PRIORITY ?= 14
GLUON_LANGS ?= de
GLUON_WLAN_MESH ?= 11s
GLUON_REGION ?= eu
GLUON_MULTIDOMAIN = 1
GLUON_DEPRECATED ?= update
