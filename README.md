<!-- SPDX-License-Identifier: Unlicense -->
<!-- SPDX-FileCopyrightText: 2023 Freifunk Chemnitz e.V. <info@chemnitz.freifunk.net> -->
# site-ffc
Gluon Site of Freifunk Chemnitz

This project uses [reuse.software](https://reuse.software/) to tell you its license.
